    :::text
    ___________________________________________________________\/\\\__       
     ___________________________________________________________\/\\\__      
      __/\\\____/\\\_____/\\\\\__/\\\\\________/\\\\\____________\/\\\__     
       _\/\\\___\/\\\___/\\\///\\\\\///\\\____/\\\///\\\_____/\\\\\\\\\__    
        _\/\\\___\/\\\__\/\\\_\//\\\__\/\\\___/\\\__\//\\\___/\\\////\\\__   
         _\/\\\___\/\\\__\/\\\__\/\\\__\/\\\__\//\\\__/\\\___\/\\\__\/\\\__  
          _\//\\\\\\\\\___\/\\\__\/\\\__\/\\\___\///\\\\\/____\//\\\\\\\/\\_ 
           __\/////////____\///___\///___\///______\/////_______\///////\//__
 

umod -- Minimalistic finite element modeling
============================================

Umod is a yet another C finite element library. It is intended to be minimal,
easy to understand, and self-contained. A simple C API is provided, planned
to become overcomplicated and awkward. 

An WebGL-based visualization application, uvis, is also being developed. Its main
design goals include demonstration-style
sliders to control various parameters of the (precalculated) solutions.
It may be used for classroom demonstration.

This whole project is intended as a sandbox for experimentation, therefore 
unsafe for any real-world application.

Requirements
------------

* CMake version 2.8.2 or higher
* Jansson 2.5